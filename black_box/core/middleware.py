import os
import csv
import sys
import json
import subprocess
import random
from os import scandir
from threading import Thread
import traceback
from time import time

def extract(ruta):
    files = [arch.name for arch in scandir(ruta) if arch.is_file()]
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    print("Trazas: ",files)
    return files
    
def repartir(archivo,num_trabajadores,algoritmo=1):
    star_time = time()
    calendario=[]
    for _ in range(num_trabajadores):
            calendario.append([])
    if algoritmo==1: #RoundRobin
        w=0
        with open("config/trazas/"+archivo,"r") as traces:
            traces_reader = csv.reader(traces, delimiter='\t')
            for t in traces_reader:
                calendario[w].append(t[0].split(' ')[0])
                w+=1
                if(w%num_trabajadores==0):
                    w=0
        #for z in range(num_trabajadores):
            #print(len(calendario[z]))
        #print("LB: {}, time: {}".format(algoritmo, time()-star_time))
    elif algoritmo==2: #PseudoAletorio
        with open("config/trazas/"+archivo,"r") as traces:
            traces_reader = csv.reader(traces, delimiter='\t')
            for t in traces_reader:
                w = random.randint(0, num_trabajadores-1)
                #print("W: {}".format(w))
                calendario[w].append(t[0].split(' ')[0])
        #for z in range(num_trabajadores):
            #print(len(calendario[z]))
        #print("LB: {}, time: {}".format(algoritmo,time()-star_time))
    elif algoritmo==3:
        with open("config/trazas/"+archivo, "r") as traces:
            traces_reader = csv.reader(traces, delimiter='\t')
            for t in traces_reader: 
                w0 = random.randint(0, num_trabajadores-1)
                w1 = random.randint(0, num_trabajadores-1)
                while w0 == w1:
                    w0 = random.randint(0, num_trabajadores-1)
                    w1 = random.randint(0, num_trabajadores-1)
                uso_w0 = len(calendario[w0])
                uso_w1 = len(calendario[w1])
                if uso_w0 >= uso_w1:
                    calendario[w1].append(t[0].split(' ')[0])
                else:
                    calendario[w0].append(t[0].split(' ')[0])
                #print("w0: {} -> uso: {}, w1: {} -> uso: {}".format(w0,uso_w0,w1,uso_w1))
    for z in range(num_trabajadores):
        print("Calendario: ",len(calendario[z]))
    print("LB: {}, time: {}".format(algoritmo, time()-star_time))
    return calendario

def calcularInterarrivos(calendario,trabajadores):
    interarrivos=[]
    for c in calendario:
        suma=0
        c.insert(0,0)
        largo=len(c)
        for n in range(largo-1):
            suma+=int(c[n+1])-int(c[n])
        z=suma/(largo-1)
        #print(z)
        interarrivos.append(z)
    return interarrivos

def client_thread(C_IP,i,C_PORT,T_TIME_SERVICE,T_DELAY):
    os.system("python3 cliente.py "+C_IP+" "+C_PORT +
              " "+str(i)+" "+T_TIME_SERVICE+" "+T_DELAY)

if __name__ == "__main__":
    #print("Args: ",sys.argv)
    #config=sys.argv[1]
    #files = [arch.name for arch in scandir("config/") if arch.is_file()]
    #if '.DS_Store' in files:
    #    files.remove('.DS_Store')
    #print("Archivos: ",files)
    #print(files)

    with open("config/config.json", 'r') as file:
        config = json.load(file)

    E_PATH = config['params']['E']['PATH']
    L_PATH = config['params']['L']['PATH']
    T_WORKERS = config['params']['T']['WORKERS']
    T_TIME_SERVICE = config['params']['T']['TIME_SERVICE']
    T_DELAY = config['params']['T']['DELAY']

    C_IP = config['params']['C']['IP']
    C_PORT = config['params']['C']['PORT']
    S_IP = config['params']['S']['IP']
    S_PORT = config['params']['S']['PORT']

    AD = config['params']['AD']
    #print(AD)

    LX = config['maps']['DAG']
    print(LX)

    for lx in LX:
        if 'E' in lx:
            print("Proceso de Extracción en proceso")
            trazas=extract(E_PATH)
            print("Proceso de Extracción finalizado")
            #print(trazas)
        elif 'S' in lx:
            print("Iniciando Servidor -> {} : {}".format(S_IP,S_PORT))
            os.system("python3 servidor.py "+S_IP+" "+S_PORT)
        elif 'C' in lx:
            print("Iniciando Clientes")
            for index, i in enumerate(interarrivos):
                #pass
                try:
                    Thread(target=client_thread, args=(
                        C_IP, i, C_PORT, T_TIME_SERVICE, T_DELAY)).start()
                    print("Trabajador {} ha concluido".format(index))
                except:
                    print("Thread did not start.")
                    traceback.print_exc()
            #print('Los clientes han concluido')
        elif 'T' in lx:
            print("Proceso de Transformación")
            calendario=repartir(trazas[0],int(T_WORKERS),AD)
            #print(len(calendario))
            interarrivos=calcularInterarrivos(calendario,int(T_WORKERS))
            print("Proceso de Transfomación finalizado")
        elif 'L' in lx:
            print("")
            print('Proceso de Carga en proceso')
            print('Proceso de Carga finalizado')
