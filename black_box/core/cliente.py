import socket
import sys
import os
import csv
import json
from os import scandir

def main(ip_server,port_server,intearrivo,service_time,delay):
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = ip_server
    port = int(port_server)

    try:
        soc.connect((host, port))
    except:
        print("Connection error")
        sys.exit()

    #print("Enter 'quit' to exit")
    message = str(intearrivo)+" "+str(service_time)

    while message != 'quit':
        soc.sendall(message.encode("utf8"))
        if soc.recv(5120).decode("utf8") == "-":
            pass        # null operation

        message = 'quit'

    soc.send(b'--quit--')

if __name__ == "__main__":
    #print(sys.argv)
    ip_server = sys.argv[1]
    port_server = sys.argv[2]
    intearrivo = sys.argv[3]
    service_time = sys.argv[4]
    delay = sys.argv[5]
    main(ip_server,port_server,intearrivo,service_time,delay)