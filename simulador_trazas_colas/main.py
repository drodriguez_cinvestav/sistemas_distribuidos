import os
import csv

path = '/Users/drodriguez/Proyectos/Cinvestav/sistemas_distribuidos/'
t_generator='trace_generator/'
q_simulator = 'queue_simulator/'

#os.system('gcc trace_generator/generator.c trace_generator/genericlib.c trace_generator/statlib.c -o trace_generator/generator -lm')
#MUESTRAS PORTION inter_arrival read_ratio SAS_SIZE DISTRIBUTION(1,2,3) mean stddev Concurrency > tracesFile.txt')
muestras='100'
portion='80'
inter_arrival=['1','10','100']
read_ratio='80'
sas_size ='100'
distribution='2'
mean='1' 
stddev='1'
concurrency='1'

#os.system('./trace_generator/generator 1000 80 20 80 100 1 2 1 1 > tracesFile.txt')
mean_inter_arrivals=[]
for ia in inter_arrival:
    os.system('./trace_generator/generator '+muestras+' '+portion+' '+ia+' '+read_ratio+' '+sas_size+' 1 2 1 1 > tracesFile'+ia+'.txt')
    
    with open('tracesFile'+ia+'.txt', 'r') as traces:
        count = 0
        acum = 0.0
        traces_reader = csv.reader(traces, delimiter='\t')
        for t in traces_reader:
            valores=t[0].split(" ")
            if count+1>=int(muestras):
                acum = int(valores[0])
            count+=1
        interrarival_time=acum/count
        mean_inter_arrivals.append(interrarival_time)
        print("Muestras: {}, Num_trazas: {}, Tiempo promedio: {}".format(muestras,count,interrarival_time))
        print("#"*40)

print(mean_inter_arrivals)

#os.system('gcc queue_simulator/single.c -o queue_simulator/single ') 
#mean_service_list=['1','10','100','1000','10000']
mean_service_list=['1','10']
for mia in mean_inter_arrivals:
    for mean_service in mean_service_list:
        mean_interarrival = str(mia)
        #mean_service = '10'
        num_delays_required = '50'
        print("mean_interarrival:{}, mean_service: {}, delay: {}".format(mean_interarrival,mean_service,num_delays_required))
        os.system('./queue_simulator/single '+mean_interarrival+' '+mean_service+' '+num_delays_required+' >> resultado_sim.txt')
        print("#"*40)

average_delay_queue=[]
with open('resultado_sim.txt') as lineas:
    for linea in lineas:
        print(linea.split(':')[0] +' -> '+linea.split(':')[1] )
        if 'Time simulation' in linea.split(':')[0]:
            average_delay_queue.append(linea.split(':')[1])

print('XXX:', average_delay_queue)