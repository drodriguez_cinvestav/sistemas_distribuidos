import socket
import sys

HEADER_LENGTH = 10

IP = socket.gethostname()
PORT = 50000

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((IP, PORT))
#client_socket.setblocking(False)

while True:
    message = input(f"iteraciones > ")

    if message:
        message = message.encode('utf-8')
        message_header = f"{len(message):< {HEADER_LENGTH}}".encode('utf-8')
        client_socket.send(message_header + message)

    try:
        message_header = client_socket.recv(HEADER_LENGTH)
        if not len(message_header):
                print("connection closed by the server")
                sys.exit()
        message_length = int(message_header.decode('utf-8').strip())
        message = client_socket.recv(message_length).decode('utf-8')
        print(message)
    except Exception as e:
        print('General error:', str(e))
        sys.exit()