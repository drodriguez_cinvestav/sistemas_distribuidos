import socket
import select
import os

HEADERSIZE = 10

########## Leer mensajes ##########
def receive_message(client_socket):
    try:
        message_header = client_socket.recv(HEADERSIZE)
        if not len(message_header):
            return False
        message_length = int(message_header.decode("utf-8").strip())
        return {"header": message_length, "data": client_socket.recv(message_length)}
    except:
        return False


IP = socket.gethostname()
PORT = 50000

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((IP,PORT))
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

server_socket.listen()

sockets_list = [server_socket]

clients = {}

while True:
    read_sockets, _, exception_sockets = select.select(sockets_list, [], sockets_list)
    for notified_socket in read_sockets:
        if notified_socket == server_socket:
            client_socket, client_address = server_socket.accept()

            sockets_list.append(client_socket)

            clients[client_socket] = client_address
            print(f"Connection from {client_address} has been established!")

        else:
            message = receive_message(notified_socket)
            if message is False:
                print(f"Closed connection from {clients[notified_socket]}")
                sockets_list.remove(notified_socket)
                del clients[notified_socket]
                continue
            print(f"Received message: {message['header']} -- {message['data'].decode('utf-8')}")
            
            msg = message['data'].decode('utf-8')
            var=msg.split(" ")
            if len(var)==2:
                interarrivo=var[0]
                tiempo_servicio=var[1]
                os.system('docker run -ti simulador_single:demo '+str(interarrivo)+' '+str(tiempo_servicio)+' 50')
            else:
                response = f"Error en los paremetros".encode('utf-8')
                response_header = f"{len(response):< {HEADERSIZE}}".encode('utf-8')
                client_socket.send(response_header + response)

            for client_socket in clients: 
                if client_socket == notified_socket:
                    response = f"I listened your petition: {message['data']}".encode('utf-8')
                    response_header = f"{len(response):< {HEADERSIZE}}".encode('utf-8')
                    client_socket.send(response_header + response)

    for notified_socket in exception_sockets:
        sockets_list.remove(notified_socket)
        del clients[notified_socket]