import socket
import sys

port = int(sys.argv[1])
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((socket.gethostname(), port))
data = input()
client.send(data.encode('utf-8'))
client.close()