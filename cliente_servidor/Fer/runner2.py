import os
import sys
import numpy as np
import shlex
import matplotlib.pyplot as plt
from subprocess import Popen, PIPE
from threading import Thread

def executeContainer(name, image, volumePath, interarrivalMean, serviceTime, fileName):
	os.system("docker run -ti -d --name "+name+ " -v "+volumePath+ " img:"+image)
	os.system("docker exec  "+name+" sh -c './single "+interarrivalMean+" " +serviceTime+" 10 > Results/"+fileName+"'")
	os.system("docker rm -f "+name)
 
 
def createResource(name, image, volumePath, port,idServer):
    os.system("docker run -ti -p "+port+":"+port+" -d --name "+name+ " -v "+volumePath+ " "+image)
    os.system("docker exec  "+name+" sh -c 'python3.6 server.py "+port+" "+idServer+"'")



resourcesNumber = input("Elige numero de recursos: ")
port = 55000
image="img:SocketsSimulator_image"
volumePath = "/home/fher/Documentos/Sockets/Results:/home/app/Results"
threads = [None] * int(resourcesNumber)
ports = [None] * int(resourcesNumber)
for i in range(len(threads)):
    thread = Thread(target=createResource, args=["Resource_"+str(i+1), image, volumePath, str(port),str(i+1)])
    thread.start()
    threads[i] = thread
    ports.append(port)
    port+=1


for t in threads:
	t.join()

"""
noSamples = sys.argv[1]
valInterarrival = sys.argv[2]
serviceTime = sys.argv[3]
workers = int (sys.argv[4])

text=[]
workSplitted = [[] for i in range(workers)]
timeSplitted = [[] for i in range(workers)]
realTimes = [[] for i in range(workers)]

file = open("trace_"+str(noSamples)+"_"+str(valInterarrival)+".txt", 'r')
for line in file.readlines()[1:]:
	text.append(line.rstrip())


for i in range(len(text)):
	timeSplitted[i%workers].append( int (text[i].split(',')[0]))

means= [None] * workers

for i in range(workers):
	counter = timeSplitted[i][0]
	counter += timeSplitted[i][1]-timeSplitted[i][0]
	for j in range(2,len(timeSplitted[i])):
		counter+= timeSplitted[i][j] - timeSplitted[i][j-1]
	
	means[i] = counter/len(timeSplitted[i])




volumePath = "/home/fher/Documentos/QueuesInContainers/Results:/home/app/Results"
dockerImage = "QueueSimulator_image"

threads = [None] * workers
for i in range(workers):
	print("Worker: "+str(i) + " Mean: "+str(means[i]))
	fileName = valInterarrival+"_"+serviceTime+"_"+str(workers)+"_"+str(i)+".txt"
	thread = Thread(target=executeContainer, args=["container_"+str(i),dockerImage,volumePath,str(means[i]),serviceTime,fileName])
	thread.start()
	threads[i] = thread


for t in threads:
	t.join()
 
 """

"""
volumePath = "/home/fher/Documentos/QueuesInContainers/Results:/home/app/Results"
dockerImage = "QueueSimulator_image"
interarrivals=[1,10,100]
services = [1,10,100,1000,10000,100000]
employees = [1,2,3,4,5,6,7,8,9,10]

for i in range(len(interarrivals)):
	file = open("trace_100_"+str(interarrivals[i])+".txt", 'r')
	text=[]
	for line in file.readlines()[1:]:
		text.append(line.rstrip())
	

	for j in range (len(services)):
		for k in range(len(employees)):
			workSplitted = [[] for z in range(employees[k])]
			timeSplitted = [[] for z in range(employees[k])]
			realTimes = [[] for z in range(employees[k])]

			
			for z in range(len(text)):
				timeSplitted[z%employees[k]].append( int (text[z].split(',')[0]))
			
			means= [None] * employees[k]

			for y in range(employees[k]):
				counter = timeSplitted[y][0]
				counter += timeSplitted[y][1]-timeSplitted[y][0]
				for z in range(2,len(timeSplitted[y])):
					counter+= timeSplitted[y][z] - timeSplitted[y][z-1]
	
				means[y] = counter/len(timeSplitted[y])

			threads = [None] * employees[k]
			
			for l in range(employees[k]):
				print("\nInterarrival: "+str(interarrivals[i])+" Service Time: "+str(services[j])+" Worker: "+str(l) + " Mean: "+str(means[l])+"\n")
				fileName = str(interarrivals[i])+"_"+str(services[j])+"_"+str(employees[k])+"_"+str(l)+".txt"
				thread = Thread(target=executeContainer, args=["container_"+str(l),dockerImage,volumePath,str(means[l]),str(services[j]),fileName])
				thread.start()
				threads[l] = thread
			
			for t in threads:
				t.join()

"""		
				
				
			
	




