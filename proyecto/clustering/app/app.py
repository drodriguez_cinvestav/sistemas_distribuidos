import os
from flask import Flask, render_template, request, flash, jsonify
from werkzeug.utils import secure_filename
from sklearn import cluster, datasets, mixture
from sklearn.metrics.cluster import adjusted_rand_score, adjusted_mutual_info_score
from sklearn import metrics
import json
from os import scandir
import numpy as np
import pandas as pd
import requests

UPLOAD_FOLDER = 'data/'
ALLOWED_EXTENSIONS = {'csv','json'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


app = Flask(__name__)
# Carpeta de subida
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route("/")
def index():
    status_code = {"server":"clustering","status_code":"200", "status_text":"Sever UP"}
    return jsonify(status_code)


@app.route("/api/v1/", methods=['POST','GET'])
def api_clustering():
    if request.method == 'POST':
        content = request.get_json(force=True)
        print(content)
        if 'cluster_type' in content.keys():
            cluster_type = content['cluster_type']
        else:
            return jsonify(error='json no es correcto')
        if 'k' in content.keys():
            k = int(content['k'])
        else:
            return jsonify(error='json no es correcto')
        if 'dataset' in content.keys():
            dataset = content['dataset']
            print(dataset)
            get_file(dataset)
        else:
            return jsonify(error='json no es correcto')
        #respuesta = {'cluster_type': cluster_type, "k": k}
        ari, mi, sh = process_data(cluster_type,k,dataset)
        respuesta = {'ARI': ari, 'MI':mi, 'SH':sh}
        return jsonify(respuesta)
    elif request.method == 'GET':
            return jsonify({"Status": "Server UP"})


@app.route('/get_file/<name>')
def get_file(name):
    #url = 'http://localhost:11000/download/'
    file_csv = requests.get('http://172.20.0.2:11000/download/'+name)
    filename = os.path.join(app.root_path, 'files', name)
    open(filename, 'wb').write(file_csv.content)
    return "200"

def process_data(clustering_type, k,dataset):
    #dfile = getData('files/')
    path = app.root_path+'/files/'
    data = pd.read_csv(path+dataset, header=1)
    X = data.iloc[:, 0:len(data.columns)-1]
    y = data.iloc[:, -1]
    if clustering_type == 'k-means':
        result = k_means(k, X, y)
    elif clustering_type == 'agglomerative':
        result = agglomerative(k, X, y)
    elif clustering_type == 'dbscan':
        result = dbscan(X, y)
    elif clustering_type == 'gmm':
        result = gmm(k, X, y)
    else:
        print("Error en Transform")
    return result

def getData(path):
    files = [arch.name for arch in scandir(path) if arch.is_file()]
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    print("Files: ", files)
    return files

def k_means(k, X, y):
    k_means = cluster.KMeans(n_clusters=k, random_state=0)
    k_means.fit(X)
    y_pred = k_means.predict(X)

    #Adjusted Rand index
    ARI = adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information", MI)
    #Silhouette Coefficient
    SH = metrics.silhouette_score(X, y_pred, metric='euclidean')
    print("SH", SH)
    return [ARI, MI, SH]

def dbscan(X, y):
    model = cluster.DBSCAN(eps=0.5, min_samples=5)
    model.fit_predict(X)
    y_pred = model.labels_.astype(np.int)
    #print(y_pred)

    #Adjusted Rand index
    ARI = adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information", MI)
    #Silhouette Coefficient
    SH = metrics.silhouette_score(X, y_pred, metric='euclidean')
    print("SH", SH)
    return [ARI, MI, SH]


def agglomerative(k, X, y):
    model = cluster.AgglomerativeClustering(n_clusters=k)
    model = model.fit(X)
    y_pred = model.labels_.astype(np.int)
    #print(y_pred)
    #Externals Index
    ARI = adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information", MI)
    #Silhouette Coefficient
    SH = metrics.silhouette_score(X, y_pred, metric='euclidean')
    print("SH", SH)

    return [ARI, MI, SH]


def gmm(k,X,y):
    gmm = mixture.GaussianMixture(n_components=k, covariance_type='full')
    y_pred = gmm.fit_predict(X,y)
    
    ARI = adjusted_rand_score(y, y_pred)
    print("Adjusted Rand Index", ARI)
    #Mutual Information
    MI = adjusted_mutual_info_score(y, y_pred)
    print("Mutual Information", MI)
    #Silhouette Coefficient
    SH = metrics.silhouette_score(X, y_pred, metric='euclidean')
    print("SH", SH)

    return [ARI, MI, SH]

   
    

if __name__ == '__main__':
    
    app.run(
        host="0.0.0.0",
        port=int("10000"),
        debug=True
    )

