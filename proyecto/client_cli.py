import requests


if __name__ == "__main__":
    cluster_type = "k-means"  # k-means agglomerative dbscan gmm
    dataset = "pca_yucatan_este5.csv"
    for k in range(2,21):
        data = {"cluster_type": cluster_type, "k": k, "dataset": dataset}
        r = requests.post('http://localhost:10000/api/v1/', json=data)
        print(r.json()['SH'])
