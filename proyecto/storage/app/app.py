from flask import Flask, render_template, request, redirect, url_for, flash, jsonify, current_app, send_from_directory
import requests
import os
from os import scandir
from os import remove
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.secret_key = 'many random bytes'
app.config['UPLOAD_FOLDER']='files/'


@app.route('/', methods=['GET'])
def index():
    status_code = {"server": "storage",
                   "status_code": "200", "status_text": "Sever UP"}
    return jsonify(status_code)


@app.route('/download/<filename>', methods=['GET'])
def download(filename):
    if request.method == 'GET':
        path = app.root_path+'/files/'
        files = getData(path)
        if filename in files:
            return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True)
        else:
            return jsonify({'status_code':'404','message': 'File not foud'})


@app.route('/delete/<filename>', methods=['GET'])
def delete(filename):
    if request.method == 'GET':
        path = app.root_path+'/files/'
        files = getData(path)
        if filename in files:
            remove(path+filename)
            return redirect(url_for('admin'))
        else:
            return jsonify({'status_code': '404', 'message': 'File not foud'})


@app.route('/files', methods=['GET'])
def getFiles():
    path = app.root_path+'/files/'
    print("PATH",path)
    files = getData(path)
    status_code = {"files": files}
    return jsonify(status_code)

def getData(path):
    files = [arch.name for arch in scandir(path) if arch.is_file()]
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    print("Files: ", files)
    return files



@app.route('/admin',methods=['GET'])
def admin():
    path = app.root_path+'/files/'
    files = getData(path)
    context ={
        'files':files
    }
    return render_template('admin.html', **context)


@app.route('/upload', methods=['POST','GET'])
def upload():
    if request.method == 'POST':
        path = app.root_path+'/files/'
        f = request.files['file']
        f.save(path+secure_filename(f.filename))
        return redirect(url_for('admin'))
    else:
        return render_template('upload.html')
      


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=11000)
