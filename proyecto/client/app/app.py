from flask import Flask, render_template, request, redirect, url_for, flash
import requests
import os
app = Flask(__name__)
app.secret_key = 'many random bytes'


@app.route('/')
def index():
    r = requests.get('http://172.20.0.2:11000/files')
    response = r.json()
    if 'files' in response.keys():
        datasets=response['files']
    print(datasets)
    context={
        'datasets':datasets
    }
    return render_template('index.html', **context)

@app.route('/clustering', methods=['POST'])
def clustering():
    flash("Clustering")
    if request.method == 'POST':
        cluster_type = request.form['clustering_type']
        k = request.form['k_form']
        dataset = request.form['dataset']
        #f = request.files['file']
        #f.save('files/'+f.filename)
        data = []
        data.append(cluster_type)
        data.append(k)

        data = {"cluster_type": cluster_type, "k": k, "dataset":dataset}
        r = requests.post('http://172.20.0.3:10000/api/v1/', json=data)
        print(r.text)
        context = {
            'data': data,
            'result': r.text
        }
    return render_template('result.html', **context)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=8080)
