import os
import csv
from os import scandir

def genTrazas():
    #os.system('gcc trace_generator/generator.c trace_generator/genericlib.c trace_generator/statlib.c -o trace_generator/generator -lm')
    #MUESTRAS PORTION inter_arrival read_ratio SAS_SIZE DISTRIBUTION(1,2,3) mean stddev Concurrency > tracesFile.txt')
    muestras = ['100','1000','10000']
    portion = '500'
    inter_arrival = ['1', '10', '100']
    read_ratio = '80'
    sas_size = '549093'
    distribution = ['1', '2', '3']
    mean = '0'
    stddev = '0'
    concurrency = '1'

    #os.system('./trace_generator/generator 1000 80 20 80 100 1 2 1 1 > tracesFile.txt')
    for muestra in muestras:
        for ia in inter_arrival:
            for d in distribution:
                if(d == '3'):
                    mean = '10'
                    stddev = '5'
                os.system('./trace_generator/generator '+muestra+' '+portion+' '+ia+' '+read_ratio +
                          ' '+sas_size+' '+d+' '+mean+' '+stddev+' 1 > trazas/dis'+d+'_m'+muestra+'_inter'+ia+'_traces.txt')


if __name__ == "__main__":
    genTrazas()
