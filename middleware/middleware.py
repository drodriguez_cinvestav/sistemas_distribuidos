import os
import csv
from os import scandir

def getArchivos(ruta):
    files = [arch.name for arch in scandir(ruta) if arch.is_file()]
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    #print("Archivos: ",files)
    return files
    
def repartir(archivo,num_trabajadores):
    calendario=[]
    for n in range(num_trabajadores):
        calendario.append([])
    w=0
    with open("trazas/"+archivo,"r") as traces:
        traces_reader = csv.reader(traces, delimiter='\t')
        for t in traces_reader:
            calendario[w].append(t[0].split(' ')[0])
            w+=1
            if(w%num_trabajadores==0):
                w=0
    #for z in range(num_trabajadores):
        #print(len(calendario[z]))
    return calendario

def calcularInterarrivos(calendario,trabajadores):
    interarrivos=[]
    for c in calendario:
        suma=0
        c.insert(0,0)
        largo=len(c)
        for n in range(largo-1):
            suma+=int(c[n+1])-int(c[n])
        z=suma/(largo-1)
        #print(z)
        interarrivos.append(z)
    return interarrivos


#Lanzando N trabajadores
if __name__ == "__main__":
    ruta="trazas/"
    archivos=getArchivos(ruta)
    archivos=sorted(archivos)
    tiempos_servicio = ['1', '10', '100']
    trabajadores=['1','2','3']
    
    for archivo in archivos:
        for worker in trabajadores:
            calendario=repartir(archivo,int(worker))
            interar=calcularInterarrivos(calendario,int(worker))

            print("Archivo: {}, Trabajadores:{}, Intearrivos:{}".format(archivo,worker,interar))
            print("#"*40)
            for i in interar:
                for t in tiempos_servicio:
                    #docker run -ti --name=w0  simulador_single:demo 10 10 5
                    print("Interarrivo: {},Tiempo de servicio: {}, Delay: 50".format(i,t))
                    os.system('docker run -ti simulador_single:demo '+str(i)+' '+str(t)+' 50')
                    print("#"*30)
