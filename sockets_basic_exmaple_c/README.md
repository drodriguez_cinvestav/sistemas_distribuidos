# sockets-basic-exmaple-c-language

Ejemplo de sockets tipo cliente y servidor en lenguaje de programación c. 

## Contenido del repositorio

* **Funciones de sockets en C** Presentación del profesor Ricardo Montañana de la Universidad de Valencia. 2012-2013. Pueden encontrar más información en https://www.uv.es/~montanan/
* **Cliente.c**: Aplicación que contiene el socket del tipo cliente que se comunica con un socket del servidor y le realiza el el envío y recepción de mensajes.
* **Servidor.c**: Aplicación que contiene el socket del tipo servidor que esta a la espera de peticiones del un socket de tipo cliente.
* **Servidorv2.c**: Aplicación que contiene el socket del tipo servidor que esta a la espera de peticiones del múltiples sockets de tipo cliente.
* **Makefile**:  Los **makefiles** son los archivos de texto que utiliza la aplicación make para llevar la gestión de la compilación de programas.

## Compilar los códigos

Para realizar la compilación de los códigos se utiliza el siguiente comando:

```
make
```

En el caso de que se deseen eliminar los binarios ya compilados se debe ejecutar el siguiente comando:

```
make clean
```

## Ejecutar los ejemplos

* **Servidor**: La aplicación *Servidor* requiere el número de puerto por el que atenderá peticiones. Por ejemplo

  ```
  ./Servidor 50000
  ```

* **Servidorv2**: Esta aplicación requiere el número de puerto por el que atenderá las peticiones y el número máximo de peticiones que puede aceptar de forma concurrente.

  ```
  ./Servidorv2 50000 100
  ```

* **Cliente**: Esta aplicación requiere la dirección ip del servidor (cualesquiera de las dos versiones) y el número de puerto. Es importante considerar las siguientes notas:

  * Alguna de las aplicaciones de servidor debe de estar habilitada
  * El cliente debe de estar en la misma red del servidor, o el servidor debe tener acceso público
  * El equipo en el que el servidor sea desplegado debe de tener accesible el puerto que se esta usando (recibir peticiones). 