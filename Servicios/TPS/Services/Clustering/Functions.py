import json
import numpy as np
import pandas as pd

def json2dataframe(data):
    df = pd.DataFrame.from_records(data)
    return df

def DF_Filter(df,filtter):
    filtter = filtter.split(",")
    return df[filtter]

def Numeric(df):
    return df.apply(pd.to_numeric)
