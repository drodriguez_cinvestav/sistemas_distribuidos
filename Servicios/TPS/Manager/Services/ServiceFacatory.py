import requests as api #for APIs request
import json
import logging #logger
class ServiceFactory(object):
    def Instance(self,name_service):
        if name_service == "describe": return describe()
        if name_service == "ANOVA": return ANOVA()     
        if name_service == "clustering": return clustering()     
        if name_service == "differential": return differential()     
        return None

    def request(self,data,options,ip):
        """
        Create a request to the microservice
        data: is the input data in the format list[{json}].
            if its necessary another format, use format_data to define a transformation function
        options: additional options for the service in json format.
        ip: ip to access to the microservice. must be defined in config.ini file

        'RETURN' json
        """
        #return type mjust be always a json
        pass

    def format_data(self,data):
        return data


"""
HERE ARE DEFINED ALL THE SERVICES WITH THE NAME USED TO BE CALLED.
"""    
class describe(ServiceFactory):

    def request(self,data,options,ip):
        body = { 'data':  data}
        url = 'http://'+ip+'/api/v1/describe'
        result = api.post(url, data=json.dumps(body))
        return result.json() #json return

class ANOVA(ServiceFactory):

    def request(self,data,options,ip):
        warn = "Please send the following parameters"
        i_params = "list of variables to apply the ANOVA"
        i_method = "correlation method: pearson, kendall, spearman (pearson by default) "
        i_example = {"variables":"Temperatura,test","method":"pearson" }
        info = {'Warning':warn, 'variables':i_params,"method":i_method, 'example':i_example}

        if 'variables' in options: attribute = options['variables']
        if 'method' in options: Met = options['method']
        else: Met = "pearson"

        body = {'data':  data}
        params = {'columns': attribute, 'method': Met}
        url = 'http://'+ip+'/api/v1/correlation'
        result = api.post(url, data=json.dumps(body), params=params)
        if result.ok:
            return result.json()
        else:
            return json.dumps(info)

class clustering(ServiceFactory):

    def request(self,data,options,ip):
        warn = "Please send the following parameters"
        i_params = "list of variables to apply the clustering"
        i_k="number of groups"
        i_group = "variable to group data (optional)"
        i_alghoritm = "clustering alghorithm: kmeans, herarhical, silhouette (kmeans by default) "
        i_example = {"K":"2","variables":"Humedad","group":"Codigo","alghoritm":"kmeans"}
        info = {'Warning':warn, "k":i_k ,'variables':i_params,"group":i_group,"alghoritm":i_alghoritm, 'example':i_example}
        try:
            if 'alghoritm' not in options: algh = "kmeans"
            else: algh = options['alghoritm']
            if not 'group' in options: group = None #data in groups (eg. date)
            else: group = options['group']
            k = options['k']
            variables = options['variables']


            url = 'http://%s/clustering/%s' %(ip,algh)
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
            Tosend = {"K":int(k),"data":data,"columns":variables,"index":group}
            result = api.post(url, data=json.dumps(Tosend),headers=headers)
            RES = result.json()
            if RES['status']=="OK":
                return RES

        except (Exception,KeyError) as e:
            print(e)
            return json.dumps(info)

class differential(ServiceFactory):

    def request(self,data,options,ip):

        warn = "Differential between 2 numeric attributtes (A-B)"
        i_A = "Attribute A"
        i_B= "Attribute B"
        i_example = {"A":"A+Temperature","B":"E+Temperature"}
        info = {'Warning':warn, "A":i_A ,'B':i_B, 'example':i_example}
        #services are not neccesary a web service

        try:
            A = options['A']
            B = options['B']
            for row in data:
                avail_keys = row.keys()
                row['differential']  = float(row[A]) - float(row[B])
            return json.dumps(data)
        except KeyError as ke:
            info['available_keys'] = list(avail_keys)
            return info
